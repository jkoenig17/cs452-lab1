/*
 * This code implements a simple shell program
 * It supports the internal shell command "exit", 
 * backgrounding processes with "&", input redirection
 * with "<" and output redirection with ">".
 * However, this is not complete.
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>


extern char **getaline();

/*
 * Handle exit signals from child processes
 */
void sig_handler(int signal) {
  int status;
  int result = wait(&status);

  printf("Wait returned %d\n", result);
}

/*
 * The main shell function
 */ 
main() {
  int i;
  char **args; 
  int result;
  int block;
  int output;
  int input;
  int append;
  int pipe;
  char *output_filename;
  char *input_filename;

  // Set up the signal handler
  sigset(SIGCHLD, sig_handler);

  // Loop forever
  while(1) {

    // Print out the prompt and get the input
    printf("->");
    args = getaline();

    // No input, continue
    if(args[0] == NULL)
      continue;

    // Check for internal shell commands, such as exit
    if(internal_command(args))
      continue;

    // Check for an ampersand
    block = (ampersand(args) == 0);

    // Check for redirected input
    input = redirect_input(args, &input_filename);

    switch(input) {
    case -1:
      printf("Syntax error!\n");
      continue;
      break;
    case 0:
      break;
    case 1:
      printf("Redirecting input from: %s\n", input_filename);
      break;
    }

    // Check for redirected output
    output = redirect_output(args, &output_filename);

    switch(output) {
    case -1:
      printf("Syntax error!\n");
      continue;
      break;
    case 0:
      break;
    case 1:
      printf("Redirecting output to: %s\n", output_filename);
      break;
    }

     // Check for appended output
    append = append_output(args, &output_filename);

    switch(append) {
    case -1:
      printf("Syntax error!\n");
      continue;
      break;
    case 0:
      break;
    case 1:
      printf("Appending output to: %s\n", output_filename);
      break;
    }
    
    pipe = isPipe(args);

    // Do the command
    do_command(args, block, 
	       input, input_filename, 
	       output, output_filename,
	       append, pipe);
  }
}

/*
 * Check for ampersand as the last argument
 */
int ampersand(char **args) {
  int i;

  for(i = 1; args[i] != NULL; i++) ;

  if(args[i-1][0] == '&') {
    free(args[i-1]);
    args[i-1] = NULL;
    return 1;
  } else {
    return 0;
  }
  
  return 0;
}

/* 
 * Check for internal commands
 * Returns true if there is more to do, false otherwise 
 */
int internal_command(char **args) {
  if(strcmp(args[0], "exit") == 0) {
    exit(0);
  }

  return 0;
}

/* 
 * Do the command
 */
int do_command(char **args, int block,
	       int input, char *input_filename,
	       int output, char *output_filename,
	       int append, int pipe) {
  
  int result;
  pid_t child_id;
  int status;

  // Fork the child process
  child_id = fork();

  // Check for errors in fork()
  switch(child_id) {
  case EAGAIN:
    perror("Error EAGAIN: ");
    return -1;
  case ENOMEM:
    perror("Error ENOMEM: ");
    return -1;
  }

  if(child_id == 0) {

    // Set up redirection in the child process
    if(input)
      freopen(input_filename, "r", stdin);
    
    if(append)
      freopen(output_filename, "a+", stdout);
    
    if(output)
      freopen(output_filename, "w+", stdout);
    
    if(pipe)
      pipeMe(args);
    

    // Execute the command
    if(pipe != 1) {
      result = execvp(args[0], args);
    
    }
    exit(-1);
    
  }

  // Wait for the child process to complete, if necessary
  if(block) {
    printf("Waiting for child, pid = %d\n", child_id);
    result = waitpid(child_id, &status, 0);
  }
}

/*
 * Check for input redirection
 */
int redirect_input(char **args, char **input_filename) {
  int i;
  int j;

  for(i = 0; args[i] != NULL; i++) {

    // Look for the <
    if(args[i][0] == '<') {
      free(args[i]);

      // Read the filename
      if(args[i+1] != NULL) {
	*input_filename = args[i+1];
      } else {
	return -1;
      }

      // Adjust the rest of the arguments in the array
      for(j = i; args[j-1] != NULL; j++) {
	args[j] = args[j+2];
      }

      return 1;
    }
  }

  return 0;
}

/*
 * Check for output redirection
 */
int redirect_output(char **args, char **output_filename) {
  int i;
  int j;

  for(i = 0; args[i] != NULL; i++) {

    // Look for the >
    if((args[i][0] == '>') && (args[i+1][0] != '>') && (args[i-1][0] != '>')) {
      
      free(args[i]);
      // Get the filename 
      if(args[i+1] != NULL) {
	*output_filename = args[i+1];
      } else {
	return -1;
      }

      // Adjust the rest of the arguments in the array
      for(j = i; args[j-1] != NULL; j++) {
	args[j] = args[j+2];
      }

      return 1;
    }
  }

  return 0;
}

/*
 * Check for output redirection
 */
int append_output(char **args, char **output_filename) {
  int i;
  int j;

  for(i = 0; args[i] != NULL; i++) {

    // Look for the >>
    if((args[i][0] == '>') && (args[i+1][0] == '>')) {
      
      free(args[i]);
      // Get the filename 
      if(args[i+2] != NULL) {
	*output_filename = args[i+2];
      } else {
	return -1;
      }

      // Adjust the rest of the arguments in the array
      for(j = i; args[j-1] != NULL; j++) {
	args[j] = args[j+3];
      }

      return 1;
    }
  }

  return 0;
}

int isPipe(char **args){
  int i = 0;
  while(args[i] != NULL){
    if(strcmp(args[i], "|") == 0){
      //THere is a '|', there's some piping going on.
      return 1;
    }
    i++;
  }
  //No match
  return 0;
}

int pipeMe(char **args){
  
  int i = 0;
  int j = 0;
  int k = 0;
  int n = 0;

      int pipesEven[2];
      int pipesOdd[2];
      int cmdsNumber = 1;
      char* command[256];      
      pid_t pid;
      
      //Calc # of commands
      while(args[i] != NULL ){
	k = 0;
	if(strcmp("|", args[i]) == 0){
	  cmdsNumber++;
	}
	i++;
      }
      //reset i
      i = 0;
      
      //Main loop
      while(args[i] != NULL){
        k = 0;
        while(strcmp(args[i], "|") != 0){
	  command[k] = args[i];
	  i++;
	  if(args[i] == NULL){
	    k++;
	    break;
	    }
	  k++;
	}
        
	//command[k] = NULL;
	i++;
	
	if(j % 2 == 0){
	  //for even j
	  pipe(pipesEven);
	}else{
	  pipe(pipesOdd);
	}
	
	 pid = fork();
	 //Check for errors
	 if(pid == -1){
	   if(j != cmdsNumber - 1){
	     //Throw some error	    
	     
	   }
	 }
	 if(pid == 0){
	   //We're the child	   	   
	  //If first command
	  if(j == 0){
	    dup2(pipesEven[1], STDOUT_FILENO);
	  }
	  //If in last command
	  else if(j == cmdsNumber - 1){
	    if(cmdsNumber % 2 != 0){
	      dup2(pipesOdd[0], STDIN_FILENO);
	    }else{
	      dup2(pipesEven[0], STDIN_FILENO);
	    }  
	    
	  }else{
	    if(j % 2 != 0){
	      dup2(pipesEven[0], STDIN_FILENO);
	      dup2(pipesOdd[1], STDOUT_FILENO);
	    }else{
	      dup2(pipesOdd[0], STDIN_FILENO);
	      dup2(pipesEven[1], STDOUT_FILENO);
	    }   
	  }
	  
	  if (execvp(command[0], command) == -1){
	    kill(getpid(),SIGTERM);
	  }
	 }
	 
        //close shit
	  if(j == 0){
	    close(pipesEven[1]);
	  }else if(j == cmdsNumber - 1){
	    if(cmdsNumber % 2 != 0){
	      close(pipesOdd[0]);
	    }else{
	      close(pipesEven[0]);
	    }
	  }else{
	    if(j % 2 != 0){
	      close(pipesEven[0]);
	      close(pipesOdd[1]);
	    }else{
	      close(pipesOdd[0]);
	      close(pipesEven[1]);
	    }
	  }

	  waitpid(pid, NULL, 0);
	  j++;
      }     
}
