To run the program:

run "flex lex.c"
Then "make"

The binary will be compiled at this point, and you can then simply run:
"./shell"

The shell should have everything implemented except for running background processes.
Piping works, and the existing errors (that were found) were fixed.

To clean the project, run "make clean".
